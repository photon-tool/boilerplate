import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { blue } from "@mui/material/colors";
import { CssBaseline } from "@mui/material";
import { MainScreen } from "../../screens/MainScreen/MainScreen";

export function App() {
  return (
    <ThemeProvider theme={createTheme({ palette: { primary: blue } })}>
      <CssBaseline />
      <Router>
        <Routes>
          <Route path="/" element={<MainScreen />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}
