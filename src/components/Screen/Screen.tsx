import { AppBar, Slide, useScrollTrigger } from "@mui/material";
import React from "react";
import styles from "./Screen.module.css";

export function ElevationScroll(props: any) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

export function HideOnScroll(props: any) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

export function Screen({ toolbar, toolbarMode = "elevate", children }: any) {
  return (
    <>
      {toolbarMode === "elevate" && (
        <ElevationScroll>
          <AppBar className={styles.photonAppBar}>{toolbar}</AppBar>
        </ElevationScroll>
      )}
      {toolbarMode === "scroll" && (
        <HideOnScroll>
          <AppBar className={styles.photonAppBar}>{toolbar}</AppBar>
        </HideOnScroll>
      )}
      {children}
    </>
  );
}
