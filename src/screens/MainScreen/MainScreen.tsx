import React from "react";
import { Box, Toolbar, Typography } from "@mui/material";
import { Screen } from "../../components/Screen/Screen";

export function MainScreen() {
  return (
    <Screen
      toolbar={
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Your App
          </Typography>
        </Toolbar>
      }
    >
      <Box my="80px">
        <Typography variant="body1" align="center">
          This is your app!
        </Typography>
      </Box>
    </Screen>
  );
}
